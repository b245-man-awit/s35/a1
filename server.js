const express = require("express");


// Mongoose is a package that allows us to create schema to model our data structure and to manipulate our database using different access methods.
const mongoose = require("mongoose");

const port = 3001;
const app = express();

// [SECTION] MongoDB connection
    // Syntax:
        // mongoose.connect("mongoDBconnectionString", {options to avoid errors in our connection})

    // link is from atlas connect > connect your application > then copy and paste ung link > change password then add folder name at "mongodb.net/folder-name?r"
    mongoose.connect("mongodb+srv://admin:admin@batch245-man-awit.8gpp4b2.mongodb.net/s35-discussion?retryWrites=true&w=majority", {
        // allows us to avoid any current and furure errors while connection to MONGODB (pag may future update si mongoDB)
        useNewUrlParser: true,
        useUnifiedTopology: true

    })

    let db = mongoose.connection;

    // error handling in connecting 
    db.on("error", console.error.bind(console, "Connection error"));

    // this will be triggered if the connection is succesful.
    db.once("open", ()=> console.log("We're connected to the cloud database!"))

    // Mongoose Schemas
        // Schemas determine the structure of the documents to be written in the database
        // achemas acts as the blueprint to our data
        // Syntax:
            // const shemaName = new mongoose.Schema({keyValuePairs})

        // taskSchema it contains 2 properties: name &status 
        // required is used to specify taht a field must not be empty

        // default - is used if a field value is not supplied

        const taskSchema = new mongoose.Schema({
            name: {
                type: String,
                required: [true, "Task name is required!"]
            },
            status: {
                type: String,
                default: "pending"
            }
        })

        const schema = new mongoose.Schema({
            username: {
                type: String,
                required: [true, "username is required!"]
            },
            password: {
                type: String,
                required: [true, "password is required!"]
            }
        })



    // [SECTION] Models
        // Uses schema to create/instantiate documents/ objects that follows our schema structure

        // the variable/object that will be created can be used to run commands with our database

        // Syntax:
            // const variableName = mongoose.model("collectionName", schemaName)


        const Task = mongoose.model("Task", taskSchema)

        const User = mongoose.model("User", schema)


// middlewares
app.use(express.json()) // Allows the app to read json data
app.use(express.urlencoded({exteded: true})) // it will allow our app to read data from forms.

// [SECTIONS] Routing
    // Create/add new task
        // 1. Check if the task is exsisting.
            // if task already exist in the database we will return a mesage "The task is already existing!"
            // if the task doesnt't exist in the database, we will add it in the database.

    app.post("/tasks", (request, response) => {
        let input = request.body

        console.log(input.status);
        if(input.status === undefined){
            Task.findOne({name: input.name}, (error, result) => {
            console.log(result);

            if (result !==null){
                return response.send("The task is already existing");
            } else{
                let newTask = new Task({
                    name: input.name
                })

                // save() method will save the object in the collection that the object instatiated
                newTask.save((saveError,savedTask) => {
                    if(saveError){
                        return console.log(saveError);
                    }
                    else{
                        return response.send("New Task created!")
                    }
                })
            }
         })
        }else{
            Task.findOne({name: input.name}, (error, result) => {
            console.log(result);

            if (result !==null){
                return response.send("The task is already existing");
            } else{
                let newTask = new Task({
                    name: input.name,
                    status: input.status
                })

                // save() method will save the object in the collection that the object instatiated
                newTask.save((saveError,savedTask) => {
                    if(saveError){
                        return console.log(saveError);
                    }
                    else{
                        return response.send("New Task created!")
                    }
                })
            }
         })
        }

    })

    // Activity

    app.post("/users", (request, response) => {
        let input = request.body
        User.findOne({username: input.username}, (error, result) => {
            console.log(result);

            if(result !== null){
                return response.send ("Username already exist!");
            }else{
                let newUser = new User({
                    username: input.username,
                    password: input.password
                })

                newUser.save((saveError, savedUser) => {
                    if(saveError){
                        return console.log(saveError);
                    }else{
                        return response.send("New user created!")
                    }
                })
            }
        })
    })

    // [Section] Retrieving the all the tasks
        app.get("/tasks", (request, response) => {
            Task.find({}, (error, result) => {
                if (error){
                    console.log(error);
                }else{
                    return response.send(result);
                }
            })
        })


      


   

    app.listen(port, ()=> console.log(`Server is running at port ${port}!`))